package com.nixsolutions.model;

import java.util.ArrayList;
import java.util.List;

import com.nixsolutions.entity.Term;

public class Data {
	
	private static List<Term> data;
	private static long count = 5;

	static {
		data = new ArrayList<Term>();
		data.add(new Term(1L, "Autumn-2014"));
		data.add(new Term(2L, "Spring-2015"));
		data.add(new Term(3L, "Autumn-2015"));
		data.add(new Term(4L, "Spring-2016"));
	}

	public static List<Term> getData() {
		return data;
	}

	public static Term findTermById(long id) {
		for (Term term : data) {
			if (term.getTermId() == id) {
				return term;
			}
		}
		return null;
	}

	public static boolean deleteTermById(long id) {
		boolean result = false;
		for (Term message : data) {
			if (message.getTermId() == id) {
				result = data.remove(message);
				return result;
			}
		}
		return result;
	}

	public static boolean updateTerm(Term message) {
		boolean result = false;
		for (Term temp: data) {
			if (temp.getTermId() == message.getTermId()) {
				temp.setTermName(message.getTermName());
				result = true;
			}
		}
		return result;
	}

	public static boolean addTerm(Term term) {
		term.setTermId(count);
		count++;
		return data.add(term);
	}

}
