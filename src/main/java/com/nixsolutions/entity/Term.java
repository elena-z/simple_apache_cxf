package com.nixsolutions.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Term {
	
	private long termId;
	private String termName;

	public Term() {

	}

	public Term(long termId, String termName) {

		this.termId = termId;
		this.termName = termName;
	}

	public long getTermId() {
		return termId;
	}

	public void setTermId(long termId) {
		this.termId = termId;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	
	@Override
	public String toString() {
		return "Term [termId=" + termId + ", termName="
				+ termName + "]";
	}		

}
