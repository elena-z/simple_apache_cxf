package com.nixsolutions.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import com.nixsolutions.entity.Term;

@Path("/")
public interface TermService {
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Term> getAllTerms();


	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Term getTermById(@PathParam("id") long id);

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public void addTerm(JAXBElement<Term> term);

	@DELETE
	@Path("{id}")
	public void deleteTerm(@PathParam("id") long id);

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public void updateTerm(JAXBElement<Term> term);

}
