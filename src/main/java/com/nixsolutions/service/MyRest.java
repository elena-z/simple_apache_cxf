package com.nixsolutions.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
public interface MyRest {

    @GET
    @Path("/simple")
    @Produces("application/xml")
    public String getRest() throws Exception;
}

