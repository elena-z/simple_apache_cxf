package com.nixsolutions.service;

import java.util.List;

import javax.xml.bind.JAXBElement;

import com.nixsolutions.entity.Term;
import com.nixsolutions.model.Data;

public class TermServiceImpl implements TermService{

	@Override
	public List<Term> getAllTerms() {
		List<Term> terms = Data.getData();
		if (terms == null) {
			throw new RuntimeException("Can't load all terms");
		}
		return terms;
	}

	@Override
	public Term getTermById(long id) {
		Term term = Data.findTermById(id);
		if (term == null) {
			throw new RuntimeException("can't find term with id = " + id);
		}
		return term;
	}

	@Override
	public void addTerm(JAXBElement<Term> term) {
		if (Data.addTerm(term.getValue()) != true) {
			throw new RuntimeException("can't add term with id = "
					+ term.getValue().getTermId());
		}
		
	}

	@Override
	public void deleteTerm(long id) {
		if (Data.deleteTermById(id) != true) {
			throw new RuntimeException("can't delete term with id = " + id);
		}
	}

	@Override
	public void updateTerm(JAXBElement<Term> term) {
		if (Data.updateTerm(term.getValue()) != true) {
			throw new RuntimeException("can't update term with id = "
					+ term.getValue().getTermId());
		}
		
	}

}
